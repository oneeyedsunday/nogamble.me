package cache

import (
	"context"
	"fmt"
	"strings"
	"sync"

	"gitlab.com/oneeyedsunday/nogamble.me/models"
)

type InMemoryFixtureCache struct {
	lock sync.RWMutex
	data []*models.Fixture
}

func NewInMemoryFixtureCache() *FixtureCache {
	var repo FixtureCache = &InMemoryFixtureCache{
		data: make([]*models.Fixture, 0, 0),
	}

	return &repo
}

func (r *InMemoryFixtureCache) Set(ctx context.Context, fixtures []*models.Fixture) error {
	if fixtures == nil {
		return models.AppError("cannot insert empty fixtures collection")
	}

	r.lock.Lock()

	defer r.lock.Unlock()

	r.data = copyData(fixtures)

	return nil
}

func (r *InMemoryFixtureCache) GetAll(ctc context.Context) ([]*models.Fixture, error) {
	r.lock.RLock()

	defer r.lock.RUnlock()

	if cap(r.data) == 0 {
		return nil, models.AppError("cache is empty")
	}

	return copyData(r.data), nil
}

func (r *InMemoryFixtureCache) GetById(ctx context.Context, id string) (*models.Fixture, error) {
	r.lock.RLock()

	defer r.lock.RUnlock()

	for _, v := range r.data {
		if strings.EqualFold(v.ID, id) {
			return copyData([]*models.Fixture{v}[:1])[0], nil
		}
	}

	return nil, fmt.Errorf("failed to get by id: %s ", id)
}

func copyData(data []*models.Fixture) []*models.Fixture {
	result := make([]*models.Fixture, len(data))
	copy(result, data)

	return result
}
