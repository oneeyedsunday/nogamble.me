package cache

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/oneeyedsunday/nogamble.me/models"
)

func Test_Set(t *testing.T) {
	cache := *NewInMemoryFixtureCache()

	d := make([]*models.Fixture, 2)

	err := cache.Set(context.TODO(), d)

	require.NoError(t, err)
}

func Test_GetAll(t *testing.T) {
	cache := *NewInMemoryFixtureCache()

	d := make([]*models.Fixture, 2)
	err := cache.Set(context.TODO(), d)
	require.NoError(t, err)

	returned, err := cache.GetAll(context.TODO())

	require.NoError(t, err)
	require.Equal(t, d, returned)
}

func Test_GetAll_EmptyCache(t *testing.T) {
	cache := *NewInMemoryFixtureCache()

	_, err := cache.GetAll(context.TODO())

	require.Error(t, err, "cache is empty")
}

func Test_GetById(t *testing.T) {
	cache := *NewInMemoryFixtureCache()

	d := make([]*models.Fixture, 1)
	d[0] = &models.Fixture{
		ID: "foo",
	}

	err := cache.Set(context.TODO(), d)
	require.NoError(t, err)

	found, err := cache.GetById(context.TODO(), "foo")
	require.NoError(t, err)

	require.Equal(t, d[0], found)
}

func Test_GetById_Not_Found(t *testing.T) {
	cache := *NewInMemoryFixtureCache()

	d := make([]*models.Fixture, 1)
	d[0] = &models.Fixture{
		ID: "foo",
	}

	err := cache.Set(context.TODO(), d)
	require.NoError(t, err)

	_, err = cache.GetById(context.TODO(), "bar")
	require.Error(t, err, "failed to get by id: bar ")
}
