package cache

import (
	"context"

	"gitlab.com/oneeyedsunday/nogamble.me/models"
)

// FixtureCache is a cache for fictures
// TODO: Make generic
type FixtureCache interface {
	Set(context.Context, []*models.Fixture) error
	GetAll(context.Context) ([]*models.Fixture, error)
	GetById(context.Context, string) (*models.Fixture, error)
}
