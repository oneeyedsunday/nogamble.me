package dto

type GetOddsFromBookieForFixtureDto struct {
	id, bookie string
}

type GetMarketOddsFromBookieForFixtureDto struct {
	GetOddsFromBookieForFixtureDto
	market string
}

func (d GetOddsFromBookieForFixtureDto) Id() string {
	return d.id
}

func (d GetOddsFromBookieForFixtureDto) Bookie() string {
	return d.bookie
}

func (m GetMarketOddsFromBookieForFixtureDto) Market() string {
	return m.market
}

func (d GetOddsFromBookieForFixtureDto) String() string {
	return d.id + ":" + d.bookie
}

func (m GetMarketOddsFromBookieForFixtureDto) String() string {
	return m.GetOddsFromBookieForFixtureDto.String() + ":" + m.market
}

func NewGetOddsFromBookieForFixtureDto(id, bookie string) GetOddsFromBookieForFixtureDto {
	return GetOddsFromBookieForFixtureDto{
		id:     id,
		bookie: bookie,
	}
}

func NewGetMarketOddsFromBookieForFixtureDto(id, bookie, market string) GetMarketOddsFromBookieForFixtureDto {
	d := NewGetOddsFromBookieForFixtureDto(id, bookie)

	return GetMarketOddsFromBookieForFixtureDto{
		GetOddsFromBookieForFixtureDto: d,
		market:                         market,
	}
}
