package models

type Sport struct {
	tracksUpdates
	Key     string `json:"key" db:"key"`
	Group   string `json:"group" db:"group"`
	Details string `json:"details" db:"details"`
	Title   string `json:"title" db:"title"`
	Active  bool   `json:"active" db:"active"`
}
