package models

import "time"

type tracksUpdates struct {
	UpdatedAt time.Time `db:"updated_at"`
}
