package models

// AppID represents an encapsulation on the identifier we use Application wide
type AppID uint16
