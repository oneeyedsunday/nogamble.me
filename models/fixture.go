package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"strings"
	"time"

	"github.com/lib/pq"
)

type dbStringSlice []string
type siteSlice []Site

// OddTime represents how the API sends back Time info
type OddTime uint64

// Odd represents how the Odds are represented e.g 1.5
type Odd float32
type odds struct {
	H2H    []Odd `json:"h2h"`
	H2HLay []Odd `json:"h2h_lay"`
}
type Site struct {
	Odds       odds    `json:"odds"`
	SiteKey    string  `json:"site_key"`
	SiteNice   string  `json:"site_nice"`
	LastUpdate OddTime `json:"last_update"`
}

type Fixture struct {
	Sites siteSlice `json:"sites" db:"sites"`
	tracksUpdates
	Teams        pq.StringArray `json:"teams" db:"teams"`
	ID           string         `json:"id" db:"id"`
	SportKey     string         `json:"sport_key" db:"sport_key"`
	SportNice    string         `json:"sport_nice" db:"sport_nice"`
	HomeTeam     string         `json:"home_team" db:"home_team"`
	CommenceTime OddTime        `json:"commence_time" db:"commence_at"`
	SitesCount   uint8          `json:"sites_count" db:"sites_count"`
}

func (p *dbStringSlice) Scan(src interface{}) error {
	if src != nil {

		s, ok := src.([]byte)
		if !ok {
			return errors.New("invlaid data expected string array")
		}

		sr := string(s[:])

		*p = strings.Split(sr[1:len(sr)-1], ",")
	}

	*p = []string{}
	return nil
}
func (p dbStringSlice) Value() (driver.Value, error) {
	return driver.Value("{" + strings.Join(p, ",") + "}"), nil
}

func (p *siteSlice) Scan(src interface{}) error {
	b, ok := src.([]byte)
	if !ok {
		return errors.New("invalid data type received")
	}

	err := json.Unmarshal(b, &p)

	return err
}
func (p siteSlice) Value() (driver.Value, error) {
	j, err := json.Marshal(p)

	if err != nil {
		return nil, errors.New("failed to convert to json")
	}
	return driver.Value(string(j)), nil
}

func (p *OddTime) Scan(src interface{}) error {
	if src == nil {
		*p = 0
		return nil
	}

	s, ok := src.(time.Time)
	if !ok {
		return errors.New("value not valid time value")
	}

	*p = OddTime(s.Unix())
	return nil
}
func (p OddTime) Value() (driver.Value, error) {
	return driver.Value(time.Unix(int64(p), 0)), nil
}

func (f *Fixture) GetBookie(key string) *Site {
	for _, v := range f.Sites {
		if strings.EqualFold(v.SiteKey, key) {
			return &v
		}
	}

	return nil
}
