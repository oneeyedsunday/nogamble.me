package services

import (
	"gitlab.com/oneeyedsunday/nogamble.me/cache"
	"gitlab.com/oneeyedsunday/nogamble.me/client"
	"gitlab.com/oneeyedsunday/nogamble.me/repository"
)

type Services struct {
	sportRepo    repository.SportRepo
	fixtureRepo  repository.FixtureRepo
	oddClient    client.OddsClient
	fixtureCache cache.FixtureCache
}

func New(sportRepo repository.SportRepo, fixtureRepo repository.FixtureRepo, oddClient client.OddsClient, fCache cache.FixtureCache) *Services {
	var s Services = Services{
		sportRepo:    sportRepo,
		fixtureRepo:  fixtureRepo,
		oddClient:    oddClient,
		fixtureCache: fCache,
	}

	return &s
}

func (s Services) SportRepo() repository.SportRepo {
	return s.sportRepo
}

func (s Services) FixtureRepo() repository.FixtureRepo {
	return s.fixtureRepo
}

func (s Services) OddClient() client.OddsClient {
	return s.oddClient
}

func (s Services) FixtureCache() cache.FixtureCache {
	return s.fixtureCache
}

var _ AppServices = (*Services)(nil)
