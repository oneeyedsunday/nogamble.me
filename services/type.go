package services

import (
	"gitlab.com/oneeyedsunday/nogamble.me/cache"
	"gitlab.com/oneeyedsunday/nogamble.me/client"
	"gitlab.com/oneeyedsunday/nogamble.me/repository"
)

type AppServices interface {
	SportRepo() repository.SportRepo
	FixtureRepo() repository.FixtureRepo
	OddClient() client.OddsClient
	FixtureCache() cache.FixtureCache
}
