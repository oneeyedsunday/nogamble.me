package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (h *Handler) NoRoute(c *gin.Context) {
	c.JSON(http.StatusNotFound, gin.H{
		"message": "not found",
	})
}
