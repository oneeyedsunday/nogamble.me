package handler

import (
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/oneeyedsunday/nogamble.me/cache"
	"gitlab.com/oneeyedsunday/nogamble.me/client"
	"gitlab.com/oneeyedsunday/nogamble.me/database"
	"gitlab.com/oneeyedsunday/nogamble.me/models"
	"gitlab.com/oneeyedsunday/nogamble.me/repository"
	"gitlab.com/oneeyedsunday/nogamble.me/services"
	"gitlab.com/oneeyedsunday/nogamble.me/usecases"
	"gitlab.com/oneeyedsunday/nogamble.me/util"
)

type Handler struct {
	s                          services.AppServices
	latestOddsUseCase          usecases.GetLatestOddsUseCase
	bookieFixtureOddsUseCase   usecases.GetOddsFromBookieForFixtureUseCase
	marketBookieFixtureUseCase usecases.GetMarketOddsFromBookieForFixtureUseCase
}

func getOddClient() (client.OddsClient, error) {
	if strings.EqualFold(os.Getenv("USE_STUB"), "true") {
		return client.NewMockedOddClient(), nil
	}

	apiKey := os.Getenv("API_KEY")

	if util.IsStringEmpty(apiKey) {
		return nil, models.AppError("apiKey not set")
	}

	c, err := client.NewClient(client.ApiKey(apiKey))

	if err != nil {
		return nil, err
	}

	return c, nil
}

func New(db *database.DB) (*Handler, error) {
	s := repository.NewSportRepo(db)
	f := repository.NewFixtureRepo(db)
	c, err := getOddClient()
	fCache := cache.NewInMemoryFixtureCache()

	if err != nil {
		return nil, err
	}

	services := services.New(*s, *f, c, *fCache)
	return &Handler{
		s:                          *services,
		latestOddsUseCase:          *usecases.NewGetLatestOddsUseCase(services),
		bookieFixtureOddsUseCase:   *usecases.NewGetOddsFromBookieForFixtureUseCase(services),
		marketBookieFixtureUseCase: *usecases.NewGetMarketOddsFromBookieForFixtureUseCase(services),
	}, nil
}

func (h *Handler) Register(v1 *gin.RouterGroup) {
	// register routes here

	oddsRouter := v1.Group("odds")
	oddsRouter.GET("", h.latestOdds)
	oddsRouter.GET("/:id/:bookie/:market", h.marketOddsFromBookieForFixtureId)
	oddsRouter.GET("/:id/:bookie", h.oddsFromBookieForFixtureId)
}

func (h *Handler) Services() services.AppServices {
	return h.s
}
