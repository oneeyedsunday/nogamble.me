package handler

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/oneeyedsunday/nogamble.me/dto"
	"gitlab.com/oneeyedsunday/nogamble.me/util"
)

func (h *Handler) latestOdds(c *gin.Context) {
	err := h.latestOddsUseCase.Run(c, nil)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failure to get latest odds",
			"err":     err.Error(),
		})
		return
	}

	l, ok := c.Get("latest")

	if !ok {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failure to get latest odds",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data":    l,
	})
}

func (h *Handler) oddsFromBookieForFixtureId(c *gin.Context) {
	id, bookie := c.Param("id"), c.Param("bookie")

	if util.IsStringEmpty(id) {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid id",
		})
		return
	}

	if util.IsStringEmpty(bookie) {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid bookie",
		})
		return
	}

	d := dto.NewGetOddsFromBookieForFixtureDto(id, bookie)

	err := h.bookieFixtureOddsUseCase.Run(c, d)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failure to get odds from bookie",
			"err":     err.Error(),
		})
		return
	}

	l, ok := c.Get(d.String())

	if !ok {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failure to get latest odds",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data":    l,
	})
}

func (h *Handler) marketOddsFromBookieForFixtureId(c *gin.Context) {
	id, bookie, market := c.Param("id"), c.Param("bookie"), c.Param("market")

	// TODO move validations into middleware
	if util.IsStringEmpty(id) {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid id",
		})
		return
	}

	if util.IsStringEmpty(bookie) {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid bookie",
		})
		return
	}

	if util.IsStringEmpty(market) {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid market",
		})
		return
	}

	if !strings.EqualFold(market, "h2h") {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "only h2h market is supported for now",
		})
		return
	}

	d := dto.NewGetMarketOddsFromBookieForFixtureDto(id, bookie, market)

	err := h.marketBookieFixtureUseCase.Run(c, d)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failure to get market odds from bookie",
			"err":     err.Error(),
		})
		return
	}

	l, ok := c.Get(d.String())

	if !ok {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failure to get latest market odds",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data":    l,
	})
}
