package usecases

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/oneeyedsunday/nogamble.me/dto"
	"gitlab.com/oneeyedsunday/nogamble.me/models"
	"gitlab.com/oneeyedsunday/nogamble.me/services"
)

type GetMarketOddsFromBookieForFixtureUseCase struct {
	s services.AppServices
}

func NewGetMarketOddsFromBookieForFixtureUseCase(svc services.AppServices) *GetMarketOddsFromBookieForFixtureUseCase {
	return &GetMarketOddsFromBookieForFixtureUseCase{
		s: svc,
	}
}

func (g *GetMarketOddsFromBookieForFixtureUseCase) Run(ctx *gin.Context, d interface{}) (err error) {
	dto, ok := d.(dto.GetMarketOddsFromBookieForFixtureDto)

	if !ok {
		return models.AppError("GetMarketOddsFromBookieForFixtureDto not provided")
	}

	latest, err := g.s.FixtureCache().GetById(ctx, dto.Id())

	if err != nil {
		log.Printf("error fetching latest fixture by id from cache %e\n", err)

		latest, err = g.s.FixtureRepo().GetById(ctx, dto.Id())

		if err != nil {
			log.Printf("error fetching latest fixture by id from db %e\n", err)

			return err
		}
	}

	bookieOdds := latest.GetBookie(dto.Bookie())

	if bookieOdds == nil {
		return models.AppError(fmt.Sprintf("failed to find market %s for bookie %s ", dto.Market(), dto.Bookie()))
	}

	ctx.Set(dto.String(), bookieOdds.Odds.H2H)

	return nil
}

var _ HttpUseCase = (*GetMarketOddsFromBookieForFixtureUseCase)(nil)
