package usecases

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/oneeyedsunday/nogamble.me/services"
)

type GetLatestOddsUseCase struct {
	s services.AppServices
}

func NewGetLatestOddsUseCase(svc services.AppServices) *GetLatestOddsUseCase {
	return &GetLatestOddsUseCase{
		s: svc,
	}
}

func (g *GetLatestOddsUseCase) Run(ctx *gin.Context, d interface{}) (err error) {
	latest, err := g.s.FixtureCache().GetAll(ctx)

	if err != nil {
		log.Printf("error fetching latest from cache %e\n", err)

		latest, err = g.s.FixtureRepo().Get(ctx)

		if err != nil {
			log.Printf("error fetching latest from db %e\n", err)

			return err
		}
	}
	ctx.Set("latest", latest)
	return nil
}

var _ HttpUseCase = (*GetLatestOddsUseCase)(nil)
