package usecases

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/oneeyedsunday/nogamble.me/models"
	"gitlab.com/oneeyedsunday/nogamble.me/services"
)

type UpdateInPlayFixturesUseCase struct {
	s     services.AppServices
	delay time.Duration
}

func NewUpdateInPlayFixturesUseCase(svc services.AppServices, d time.Duration) *UpdateInPlayFixturesUseCase {
	return &UpdateInPlayFixturesUseCase{
		delay: d,
		s:     svc,
	}
}

func (u *UpdateInPlayFixturesUseCase) fetchAndSaveOnInterval(ctx context.Context) {
	// open channel that results will be sent through
	// read from channel
	// in this case we dont need to run the fetch immediately
	// since init is also running

	type fetchResult struct {
		fetched []*models.Fixture
		err     error
	}
	fetchDone := make(chan fetchResult, 1)

	t := time.NewTicker(u.delay)
	var err error
	var fetched []*models.Fixture

	for {

		select {
		case <-t.C:
			log.Printf("we are in a fetch loop")
			go func() {
				fetched, err = u.s.OddClient().GetUpcomingFixtures(ctx)
				fetchDone <- fetchResult{fetched, err}
			}()

		case result := <-fetchDone:
			fetched, err = result.fetched, result.err

			if err != nil {
				// error, do not send since no items to store and all that
				log.Println(fmt.Errorf("we had an error fetching data %w", err))
				break
			}

			log.Printf("recived new stuff: %d\n", len(fetched))
			go func() {
				// Store data in memory cache
				err = u.s.FixtureCache().Set(ctx, fetched)
				// Upsert Db data
				err = u.s.FixtureRepo().Upsert(ctx, fetched)
			}()
		case <-ctx.Done():
			log.Println("ctx is done; we should stop fetching stuff")
			t.Stop()
			return
		}
	}
}

// Run updates Odds for matches which are in-play, saving results to DB
func (u *UpdateInPlayFixturesUseCase) Run(ctx context.Context) (err error) {
	// should return an error if it fails to setup
	// if failed to perform an action
	// should keep going but log errors

	// upcomingFixtures, err := u.s.OddClient().GetUpcomingFixtures(ctx)

	// call
	// hold latest in memory
	// save updates to DB (Upsert func needed)
	// schedule next call

	go u.fetchAndSaveOnInterval(ctx)

	// two major parts
	// fetching
	// updating state
	// to communicate via chanel
	return nil
}

var _ UseCase = (*UpdateNotInPlayFixturesUseCase)(nil)
