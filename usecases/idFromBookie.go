package usecases

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/oneeyedsunday/nogamble.me/dto"
	"gitlab.com/oneeyedsunday/nogamble.me/models"
	"gitlab.com/oneeyedsunday/nogamble.me/services"
)

type GetOddsFromBookieForFixtureUseCase struct {
	s services.AppServices
}

func NewGetOddsFromBookieForFixtureUseCase(svc services.AppServices) *GetOddsFromBookieForFixtureUseCase {
	return &GetOddsFromBookieForFixtureUseCase{
		s: svc,
	}
}

func (g *GetOddsFromBookieForFixtureUseCase) Run(ctx *gin.Context, d interface{}) (err error) {
	dto, ok := d.(dto.GetOddsFromBookieForFixtureDto)

	if !ok {
		return models.AppError("GetOddsFromBookieForFixtureDto not provided")
	}

	latest, err := g.s.FixtureCache().GetById(ctx, dto.Id())

	if err != nil {
		log.Printf("error fetching latest fixture by id from cache %e\n", err)

		latest, err = g.s.FixtureRepo().GetById(ctx, dto.Id())

		if err != nil {
			log.Printf("error fetching latest fixture by id from db %e\n", err)

			return err
		}
	}

	ctx.Set(dto.String(), latest.GetBookie(dto.Bookie()))

	return nil
}

var _ HttpUseCase = (*GetOddsFromBookieForFixtureUseCase)(nil)
