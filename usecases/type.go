package usecases

import (
	"context"

	"github.com/gin-gonic/gin"
)

type UseCase interface {
	Run(ctx context.Context) error
}

type HttpUseCase interface {
	Run(ctx *gin.Context, data interface{}) error
}
