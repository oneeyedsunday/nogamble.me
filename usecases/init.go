package usecases

import (
	"context"
	"errors"
	"fmt"
	"log"

	"gitlab.com/oneeyedsunday/nogamble.me/models"
	"gitlab.com/oneeyedsunday/nogamble.me/services"
)

type InitializerUseCase struct {
	s services.AppServices
}

func NewInitializerUseCase(svc services.AppServices) *InitializerUseCase {
	return &InitializerUseCase{s: svc}
}

func (i *InitializerUseCase) getAndSaveSports(ctx context.Context) error {
	cErr := make(chan error, 1)
	cErr2 := make(chan error, 1)

	go func() {
		sports, err := i.s.OddClient().GetSports(ctx)

		if err != nil {
			cErr <- models.AppError("failed to get sports")
		}

		err = i.s.SportRepo().Upsert(ctx, sports)

		if err != nil {
			cErr <- models.AppError("failed to save sports")
		}

		cErr <- nil
	}()

	go func() {
		upcomingFixtures, err := i.s.OddClient().GetUpcomingFixtures(ctx)
		if err != nil {
			cErr2 <- models.AppError("failed to get upcoming fixtures")
		}

		err = i.s.FixtureRepo().Upsert(ctx, upcomingFixtures)

		if err != nil {
			cErr2 <- models.AppError("failed to save upcoming fixtures")
		}

		cErr2 <- nil
	}()

	err := <-cErr
	err2 := <-cErr2

	if err != nil {
		return err
	}

	if err2 != nil {
		return err2
	}

	return nil
}

// Run in the InitializerUseCase, fetches and stores all available sports
// and all upcoming fixtures in the database
func (i *InitializerUseCase) Run(ctx context.Context) (err error) {

	err = i.getAndSaveSports(ctx)

	if err != nil {
		fmt.Printf("logic failed %v\n", errors.Unwrap(err))
	}

	log.Println("completed InitializerUseCase")

	return
}

var _ UseCase = (*InitializerUseCase)(nil)
