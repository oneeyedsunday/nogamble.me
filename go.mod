module gitlab.com/oneeyedsunday/nogamble.me

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/google/go-querystring v1.1.0
	github.com/gwuah/postmates v0.0.0-20210110205606-3c978672f2d0
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.6
	github.com/stretchr/testify v1.8.0
)
