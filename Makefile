test:
	go test ./...
build:
	go generate
	go build main.go
run:
	go generate
	go run main.go
