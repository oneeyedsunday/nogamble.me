CREATE TABLE IF NOT EXISTS "sports" (
    updated_at timestamp NOT NULL DEFAULT current_timestamp,
    "key" VARCHAR NOT NULL PRIMARY KEY,
    active bool NOT NULL,
    "group" VARCHAR NOT NULL,
    details VARCHAR NOT NULL,
    title VARCHAR NOT NULL
);
CREATE TABLE IF NOT EXISTS "fixtures" (
    updated_at timestamp NOT NULL DEFAULT current_timestamp,
    id VARCHAR NOT NULL PRIMARY KEY,
    sport_key VARCHAR NOT NULL,
    sport_nice VARCHAR,
    commence_at TIMESTAMP NOT NULL,
    teams VARCHAR [],
    home_team VARCHAR,
    sites_count INTEGER,
    sites JSON
);
TRUNCATE "sports";
TRUNCATE "fixtures";