package repository

import (
	"context"
	"log"

	"gitlab.com/oneeyedsunday/nogamble.me/database"
	"gitlab.com/oneeyedsunday/nogamble.me/models"
)

type PostgresSportRepo struct {
	*database.DB
}

func NewSportRepo(db *database.DB) *SportRepo {
	var repo SportRepo = &PostgresSportRepo{db}

	return &repo
}

func (r *PostgresSportRepo) Upsert(ctx context.Context, sports []*models.Sport) error {
	if sports == nil {
		return models.AppError("cannot insert empty sport collection")
	}

	_, err := r.DB.NamedExec(`INSERT INTO sports ("key", active, "group", details, title)
	VALUES (:key, :active, :group, :details, :title)
	ON CONFLICT (key) DO UPDATE
	SET active = EXCLUDED.active, "group" = EXCLUDED."group", details = EXCLUDED.details, title = EXCLUDED.title, updated_at = CURRENT_TIMESTAMP `, sports)

	if err != nil {
		log.Default().Printf("failed to insert sport entries %s", err)

		return models.AppError("failed to insert sport entries")
	}

	return nil
}

var _ SportRepo = (*PostgresSportRepo)(nil)
