package repository

import (
	"context"

	"gitlab.com/oneeyedsunday/nogamble.me/models"
)

type SportRepo interface {
	Upsert(context.Context, []*models.Sport) error
}

type FixtureRepo interface {
	Upsert(context.Context, []*models.Fixture) error
	Get(context.Context) ([]*models.Fixture, error)
	GetById(context.Context, string) (*models.Fixture, error)
}
