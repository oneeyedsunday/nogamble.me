package repository

import (
	"context"
	"log"

	"gitlab.com/oneeyedsunday/nogamble.me/database"
	"gitlab.com/oneeyedsunday/nogamble.me/models"
)

type PostgresFixtureRepo struct {
	*database.DB
}

func NewFixtureRepo(db *database.DB) *FixtureRepo {
	var repo FixtureRepo = &PostgresFixtureRepo{db}

	return &repo
}

func (r *PostgresFixtureRepo) Upsert(ctx context.Context, fixtures []*models.Fixture) error {
	if fixtures == nil {
		return models.AppError("cannot insert empty fixtures collection")
	}

	_, err := r.DB.NamedExec(`INSERT INTO fixtures (id, sport_key, sport_nice, commence_at, teams, home_team, sites_count, sites)
	VALUES (:id, :sport_key, :sport_nice, :commence_at, :teams, :home_team, :sites_count, :sites)
	ON CONFLICT (id) DO UPDATE
	SET sport_key = EXCLUDED.sport_key, sport_nice = EXCLUDED.sport_nice, commence_at = EXCLUDED.commence_at,
	 teams = EXCLUDED.teams, home_team = EXCLUDED.home_team,
	  sites_count = EXCLUDED.sites_count, sites = EXCLUDED.sites, updated_at = CURRENT_TIMESTAMP `, fixtures)

	if err != nil {
		log.Default().Printf("failed to insert fixture entries %s", err)

		return models.AppError("failed to insert fixture entries")
	}
	return nil
}

func (r *PostgresFixtureRepo) Get(ctx context.Context) ([]*models.Fixture, error) {
	var f []*models.Fixture

	err := r.DB.SelectContext(ctx, &f, "select * from fixtures")

	if err != nil {
		log.Default().Printf("failed to get fixtures from db %e\n", err)

		return nil, models.AppError("failed to get fixture entries")
	}

	return f, nil
}

func (r *PostgresFixtureRepo) GetById(ctx context.Context, id string) (*models.Fixture, error) {
	var f []*models.Fixture

	err := r.DB.SelectContext(ctx, &f, `select * from fixtures where id = $1 limit 1`, id)

	if err != nil {
		log.Default().Printf("failed to get fixture by key from db %e\n", err)

		return nil, models.AppError("failed to get fixture by key")
	}

	return f[0], nil
}
