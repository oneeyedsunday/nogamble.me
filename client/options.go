package client

import (
	"net/http"
	"time"
)

// Option is a configuration type
type Option func(*Client)

// HTTPClient allows setting base httpClient
func HTTPClient(h *http.Client) Option {
	return func(c *Client) {
		c.c = h
	}
}

// UserAgent allows setting userAgent
func UserAgent(s string) Option {
	return func(c *Client) {
		c.userAgent = s
	}
}

// ApiBaseURL allows setting of base api Url
func APIBaseURL(s string) Option {
	return func(c *Client) {
		c.apiBaseUrl = s
	}
}

// ApiKey allows you set the key required to communicate with the API
func ApiKey(s string) Option {
	return func(c *Client) {
		c.apiKey = s
	}
}

// Timeout allows setting timeout for APi calls
func Timeout(t time.Duration) Option {
	return func(c *Client) {
		c.c.Timeout = t
	}
}

func setDefaultOptions(c *Client) {
	c.c = &http.Client{
		Timeout: time.Second * 15,
	}

	c.apiBaseUrl = defaultBaseURL
	c.userAgent = defaultUA
}
