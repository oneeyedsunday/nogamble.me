package client

import (
	"context"
	"encoding/json"
	"log"

	"gitlab.com/oneeyedsunday/nogamble.me/models"

	_ "embed"
)

//go:generate cp -r ../mocks/v3 ./local-asset-dir
//go:embed local-asset-dir/sports.json
var mockSports []byte

//go:generate cp -r ../mocks/v3 ./local-asset-dir
//go:embed local-asset-dir/odds.json
var mockFixtures []byte

type mockedClient struct{}

func NewMockedOddClient() *mockedClient {
	return &mockedClient{}
}

func (c *mockedClient) GetSports(ctx context.Context) ([]*models.Sport, error) {
	log.Println("mocking GetSports()")
	var resp getSportsResponse

	if err := json.Unmarshal(mockSports, &resp); err != nil {
		return nil, err
	}

	return resp.Data, nil
}

func (c *mockedClient) GetUpcomingFixtures(ctx context.Context) ([]*models.Fixture, error) {
	log.Println("mocking GetUpcomingFixtures()")
	var resp getFixturesResponse

	if err := json.Unmarshal(mockFixtures, &resp); err != nil {
		return nil, err
	}

	return resp.Data, nil
}

var _ OddsClient = (*mockedClient)(nil)
