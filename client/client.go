package client

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/google/go-querystring/query"
	"gitlab.com/oneeyedsunday/nogamble.me/models"
	"gitlab.com/oneeyedsunday/nogamble.me/util"
)

type Client struct {
	c          *http.Client
	apiKey     string
	userAgent  string
	apiBaseUrl string
}

const (
	defaultBaseURL = "https://api.the-odds-api.com/v3/"
	defaultUA      = "nogamble.me/go-client"
)

// Response embeds the standard response struct and might in the future include
// more detailed metadata about the response
type Response struct {
	*http.Response
}
type apiResponse struct {
	Success bool   `json:"success"`
	Message string `json:"msg"`
}

type getSportsResponse struct {
	Data []*models.Sport `json:"data"`
	apiResponse
}

type oddsQuery struct {
	Sport  string `url:"sport"`
	Region string `url:"region"`
	Market string `url:"mkt"`
}

type getFixturesResponse struct {
	Data []*models.Fixture `json:"data"`
	apiResponse
}

func (a apiResponse) IsSuccessful() bool { return a.Success }

func NewClient(opts ...Option) (*Client, error) {
	c := new(Client)
	setDefaultOptions(c)

	for _, v := range opts {
		v(c)
	}

	return c, c.validate()
}

func (c *Client) validate() error {
	if util.IsStringEmpty(c.apiKey) {
		return errors.New("please provide your api key")
	}

	return nil
}

// NewRequest creates a new http.Request for the current operation
func (c *Client) NewRequest(method string, u string, v interface{}) (*http.Request, error) {
	if strings.HasPrefix(u, "/") {
		return nil, errors.New("url shouldn't start with /")
	}

	var body = bytes.NewBuffer(nil)

	if v != nil {
		if err := json.NewEncoder(body).Encode(v); err != nil {
			return nil, err
		}
	}

	uP, err := url.Parse(u)
	if err != nil {
		return nil, err
	}

	queryString := uP.Query()
	queryString.Set("apiKey", c.apiKey)
	uP.RawQuery = queryString.Encode()

	apiBase, err := url.Parse(c.apiBaseUrl)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, apiBase.ResolveReference(uP).String(), body)

	if err != nil {
		return nil, err
	}

	if v != nil {
		req.Header.Add("Content-Type", "application/json")
	}

	req.Header.Add("User-Agent", c.userAgent)

	return req, nil
}

// Do makes the HTTP request and writes the body of the response into v
func (c *Client) Do(ctx context.Context, req *http.Request, v interface{}) (*Response, error) {

	req = req.WithContext(ctx)

	resp, err := c.c.Do(req)
	if err != nil {
		return nil, err
	}

	if err != nil {
		// check if context has been canceled
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}

		if e, ok := err.(*url.Error); ok {
			return nil, e
		}

		// Return err as is
		return nil, err
	}

	defer resp.Body.Close()

	if n := resp.StatusCode; n != http.StatusOK && n != http.StatusCreated {
		var d struct {
			Message string `json:"msg"`
			Status  string `json:"status"`
		}

		if err := json.NewDecoder(resp.Body).Decode(&d); err != nil {
			return nil, err
		}

		return nil, errors.New(d.Message)
	}

	// this is potentially bad with large payloads
	// see https://haisum.github.io/2017/09/11/golang-ioutil-readall/
	respCopy, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	logToFile := os.Getenv("LOG_RESPONSES_TO_FILE")
	if strings.EqualFold(logToFile, "true") {
		// do this in goroutine
		copyResponseToFile(bytes.NewBuffer(respCopy), req.URL.Path)
	}

	if v != nil {
		if err := json.NewDecoder(bytes.NewBuffer(respCopy)).Decode(v); err != nil {
			return nil, err
		}
	}

	return &Response{resp}, err
}

func checkClose(c io.Closer, err *error) {
	cerr := c.Close()
	if *err == nil {
		*err = cerr
	}
}

// copyResponseToFile lets up copy the result of a call to a file
// its a way to get around API limit
func copyResponseToFile(body io.Reader, resource string) (err error) {
	saveFile := filepath.Join(os.Getenv("LOG_FILE_BASE_PATH"), resource+".json")
	out, err := os.Create(saveFile)

	if err != nil {
		log.Println(fmt.Errorf("failed to open file for writing responses %w", err))
		return
	}

	defer checkClose(out, &err)

	_, err = io.Copy(out, body)

	if err != nil {
		log.Println(fmt.Errorf("failed to copy response of %s to file %s %w", resource, saveFile, err))
	}
	return
}

func addQueryString(s string, opts interface{}) (string, error) {
	v := reflect.ValueOf(opts)
	if v.Kind() == reflect.Ptr && v.IsNil() {
		return s, nil
	}

	u, err := url.Parse(s)
	if err != nil {
		return s, err
	}

	qs, err := query.Values(opts)
	if err != nil {
		return s, err
	}

	u.RawQuery = qs.Encode()
	return u.String(), nil
}

func (c *Client) GetSports(ctx context.Context) ([]*models.Sport, error) {
	req, err := c.NewRequest(http.MethodGet, "sports", nil)
	if err != nil {
		return nil, err
	}

	var sports = new(getSportsResponse)

	_, err = c.Do(ctx, req, &sports)
	if err != nil {
		return nil, err
	}

	return sports.Data, nil
}

func (c *Client) GetUpcomingFixtures(ctx context.Context) ([]*models.Fixture, error) {
	withQuery, err := addQueryString("odds", oddsQuery{
		Sport:  "upcoming",
		Region: "uk",
		Market: "h2h",
	})

	if err != nil {
		return nil, err
	}
	req, err := c.NewRequest(http.MethodGet, withQuery, nil)

	if err != nil {
		return nil, err
	}

	var fixtures = new(getFixturesResponse)

	_, err = c.Do(ctx, req, &fixtures)
	if err != nil {
		return nil, err
	}

	return fixtures.Data, nil
}

var _ OddsClient = (*Client)(nil)
