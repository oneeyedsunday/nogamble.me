package client

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/oneeyedsunday/nogamble.me/models"
)

type roundTripper struct {
	successResponseBody interface{}
	failureResponseBody apiResponse
	statusCode          uint
	isFailure           bool
}

var nilResp *Response

var dummyKey string = "oo"

func (rt *roundTripper) RoundTrip(r *http.Request) (*http.Response, error) {
	rr, w := io.Pipe()

	reqBodyBytes := new(bytes.Buffer)

	if rt.isFailure {
		json.NewEncoder(reqBodyBytes).Encode(rt.failureResponseBody)
	} else {
		json.NewEncoder(reqBodyBytes).Encode(rt.successResponseBody)
	}

	resp := &http.Response{
		Request:    r,
		StatusCode: int(rt.statusCode),
		Body:       rr,
	}

	go func() {
		defer w.Close()
		io.Copy(w, reqBodyBytes)
	}()

	return resp, nil
}

func NewRoundTripperWithResponse(resp interface{}) *roundTripper {
	return &roundTripper{
		successResponseBody: resp,
		statusCode:          200,
	}
}

func NewRoundTripperWithFailureResponse(statusCode uint, message string) *roundTripper {
	return &roundTripper{
		failureResponseBody: apiResponse{
			Success: false,
			Message: message,
		},
		statusCode: statusCode,
		isFailure:  true,
	}
}

func test_Url(t *testing.T, u1, u2 string) {
	url1, err := url.ParseRequestURI(u1)

	require.NoError(t, err)

	url2, err := url.ParseRequestURI(u2)

	require.NoError(t, err)
	require.Equal(t, url1.Opaque, url2.Opaque)
}

func Test_Currencies(t *testing.T) {
	cTests := []struct {
		Name               string
		Query              string
		ExpectedModels     interface{}
		ExpectedMethod     string
		ExpectedRequestUrl string
	}{
		{
			Name:               "sports",
			ExpectedModels:     make([]*models.Sport, 2),
			ExpectedMethod:     http.MethodGet,
			Query:              "sports",
			ExpectedRequestUrl: fmt.Sprintf("%s%s?apiKey=%s", defaultBaseURL, "sports", dummyKey),
		},
		{
			Name:               "with-testnet-network-filter-true",
			ExpectedModels:     make([]*models.Fixture, 1),
			ExpectedMethod:     http.MethodGet,
			Query:              "fixtures?sport=upcoming&region=uk&mkt=h2h",
			ExpectedRequestUrl: fmt.Sprintf("%s%s?apiKey=%s&sport=upcoming&region=uk&mkt=h2h", defaultBaseURL, "fixtures", dummyKey),
		},
	}

	for _, tt := range cTests {
		type respType struct {
			Data interface{} `json:"data"`
			apiResponse
		}
		h := &http.Client{
			Timeout: time.Second * 5,
			Transport: NewRoundTripperWithResponse(respType{
				apiResponse: apiResponse{
					Success: true,
				},
				Data: tt.ExpectedModels,
			}),
		}
		t.Run(tt.Name, func(t *testing.T) {
			c, _ := NewClient(HTTPClient(h), ApiKey(dummyKey))
			req, err := c.NewRequest(http.MethodGet, tt.Query, nil)

			require.NoError(t, err)

			var models respType

			resp, err := c.Do(context.TODO(), req, &models)

			require.NoError(t, err)
			require.Equal(t, []byte(fmt.Sprint(models.Data)), []byte(fmt.Sprint(tt.ExpectedModels)))
			require.Equal(t, resp.Request.Method, http.MethodGet)
			test_Url(t, resp.Request.URL.String(), tt.ExpectedRequestUrl)
		})
	}
}

func Test_Client_Errors(t *testing.T) {
	cFTests := []struct {
		Name            string
		StatusCode      uint
		ApiErrorMessage string
	}{
		{
			Name:            "Bad-Request",
			StatusCode:      400,
			ApiErrorMessage: "Bad Filter",
		},
		{
			Name:            "Unauthroized",
			StatusCode:      401,
			ApiErrorMessage: "Unauthorized",
		}, {
			Name:            "Internal Server Error",
			StatusCode:      500,
			ApiErrorMessage: "Internal Server Error",
		},
	}

	for _, tt := range cFTests {
		h := &http.Client{
			Timeout:   time.Second * 5,
			Transport: NewRoundTripperWithFailureResponse(tt.StatusCode, tt.ApiErrorMessage),
		}
		t.Run(tt.Name, func(t *testing.T) {
			c, _ := NewClient(HTTPClient(h), ApiKey("oo"))
			withQuery := "odds?sport=upcoming&region=uk&mkt=h2h"

			req, err := c.NewRequest(http.MethodGet, withQuery, nil)

			require.NoError(t, err)

			resp, err := c.Do(context.TODO(), req, nil)

			require.Error(t, err)
			require.Equal(t, err.Error(), tt.ApiErrorMessage)
			require.Equal(t, resp, nilResp)
		})

	}
}
