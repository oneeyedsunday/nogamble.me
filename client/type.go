package client

import (
	"context"

	"gitlab.com/oneeyedsunday/nogamble.me/models"
)

type OddsClient interface {
	// GetSports gets all available sports
	GetSports(context.Context) ([]*models.Sport, error)
	// GetUpcomingFixtures gets all upcoming fixtures, regardless of Sport
	GetUpcomingFixtures(context.Context) ([]*models.Fixture, error)
}
