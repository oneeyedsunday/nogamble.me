FROM golang:1.16-alpine AS main-env
# RUN apk add build-base


RUN mkdir /app
ARG PORT=9000
ENV PORT=${PORT}
ADD . /app/


WORKDIR /app
RUN cd /app
# Attempt to cache the module retrieval
RUN go mod download
RUN go build -o nogamble

FROM alpine

WORKDIR /app
COPY --from=main-env /app/nogamble /app
COPY .env /app/.env

EXPOSE $PORT

CMD ["/app/nogamble"]