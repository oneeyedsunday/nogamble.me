package database

import (
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func MustDB(conn string) *DB {

	db := sqlx.MustConnect("postgres", conn)

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(5)
	db.SetMaxIdleConns(1)

	return &DB{db}
}
