# NoGamble.Me

You can boot up a local db (and app) by running

```sh
docker-compose -p nogamble.me -f docker-compose.yml up -d
```

### Environment Variables for app
`PORT` specifies the port the app should run on

`DB_CONN_STRING` database connection string
`API_KEY` apiKey for [The Odds api](https://the-odds-api.com/liveapi/guides/v3/) 
`LOG_RESPONSES_TO_FILE` (true) specifies app should keep api responses is a file, so it can be used to stub the client later on
`LOG_FILE_BASE_PATH` path to save logged api responses to
`USE_STUB` whether or not to stub the Odds api (with saved responses in `LOG_FILE_BASE_PATH`)


#### For local (containerized) db setup only
`DB_PASS` database password (used only for)
`DB_NAME` database name to use (will be created on container startup)
`DB_USER` database user to use (will be created on container startup)
