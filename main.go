package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	_ "embed"

	"github.com/joho/godotenv"

	"gitlab.com/oneeyedsunday/nogamble.me/database"
	"gitlab.com/oneeyedsunday/nogamble.me/handler"
	"gitlab.com/oneeyedsunday/nogamble.me/server"
	"gitlab.com/oneeyedsunday/nogamble.me/usecases"
	"gitlab.com/oneeyedsunday/nogamble.me/util"
)

//go:embed db/__init.sql
var schema string

func main() {
	ENV := os.Getenv("ENV")
	if util.IsStringEmpty(ENV) {
		err := godotenv.Load()

		if err != nil {
			log.Fatal("Error loading .env file", err)
		}
	}

	dbDsn := os.Getenv("DB_CONN_STRING")
	if util.IsStringEmpty(dbDsn) {
		log.Fatalf("Error could not find db connection string")
	}

	db := database.MustDB(dbDsn)

	db.MustExec(schema)

	s := server.New()
	h, err := handler.New(db)

	if err != nil {
		log.Fatal(fmt.Errorf("failed to setup App %w\n", err))
	}

	routes := s.Group("/api/v1")
	h.Register(routes)
	s.NoRoute(h.NoRoute)

	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*5)
	defer cancelFunc()

	err = usecases.NewInitializerUseCase(h.Services()).Run(ctx)

	if err != nil {
		log.Fatal(fmt.Errorf("failed to initialize app %w", err))
	}

	errInPlay := usecases.NewUpdateInPlayFixturesUseCase(h.Services(), time.Second*2).Run(ctx)

	if errInPlay != nil {
		log.Fatal(fmt.Errorf("failed to set up in Play odds update %w", err))
	}

	fmt.Println("ok we good")

	server.Start(&s, &server.Config{
		Port: fmt.Sprintf(":%s", os.Getenv("PORT")),
	})
}
